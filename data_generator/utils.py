import dateutil.parser as dtparser


def zipf(n):
    """
    :param n: Number of elements in the array
    :return: An array of probabilities where the first the probability is twice the second, which is twice the third
    and so on.
    """
    return [2 ** (n - 1 - i) / (2 ** n - 1) for i in range(n)]


class IdGenerator:
    def __init__(self):
        self._last_id = 0

    def generate(self):
        self._last_id += 1
        return self._last_id


def try_dtparse(val):
    try:
        if val:
            return dtparser.parse(val)
    except:
        pass
    return None