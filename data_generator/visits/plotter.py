from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import data_generator.visits.generator as visitsgen


def plot_sessions(visits):
    print('Generating visits distribution by session')
    sids = [visit[1] for visit in visits]
    sids_freq = Counter(sids)
    sess_visit_counts = np.array(list(sids_freq.values()))
    plt.hist(sess_visit_counts)
    plt.xlabel('Visit Counts')
    plt.ylabel('Number of Sessions')
    plt.show()


def plot_games(visits):
    print('Generating visits distribution by games')
    allgids = [visit[2] for visit in visits]
    gidsfreq = Counter(allgids)
    gidsfreq = sorted(gidsfreq.items(), key=lambda x: x[1], reverse=True)
    gids = [g[0] for g in gidsfreq]
    freq = [g[1] for g in gidsfreq]
    num_bars = len(gids)
    plt.bar(range(num_bars), freq, align='center')
    plt.xticks(range(num_bars), gids)
    plt.xlabel('Games')
    plt.ylabel('Visit Counts')
    plt.show()


def main():
    _, visits = visitsgen.workflow(100000, '/home/avilay/data/ecomm/sessions.psv', '/home/avilay/data/ecomm/games_med.psv')
    plot_sessions(visits)
    plot_games(visits)


if __name__ == '__main__':
    main()
