import csv
import logging
import os.path as path
import re
import requests
import numpy as np
from bs4 import BeautifulSoup
from data_generator.parts.videogames.headers import *
from data_generator.utils import IdGenerator, try_dtparse


logger = logging.getLogger(__name__)


class VideoGame:
    def __init__(self, **kwargs):
        self.product_id = kwargs['product_id']
        self.title = kwargs['title'] if 'title' in kwargs else None
        self.released_on = kwargs['released_on'] if 'released_on' in kwargs else None
        self.price = kwargs['price'] if 'price' in kwargs else None
        self.platforms = set()
        self.genres = set()
        self.developers = set()
        self.publishers = set()


class Platform:
    def __init__(self, **kwargs):
        self.platform_id = kwargs['platform_id']
        self.name = kwargs['name']


class Genre:
    def __init__(self, **kwargs):
        self.genre_id = kwargs['genre_id']
        self.name = kwargs['name']


class Developer:
    def __init__(self, **kwargs):
        self.developer_id = kwargs['developer_id']
        self.name = kwargs['name']


class Publisher:
    def __init__(self, **kwargs):
        self.publisher_id = kwargs['publisher_id']
        self.name = kwargs['name']


def process(row, headers):
    cells = row.find_all('td')
    if len(cells) == 0:
        return None

    # Ensure that cells is at least max_index in length
    max_index = max(headers.values())
    filler = [None] * (max_index + 1 - len(cells))
    cells.extend(filler)

    game = dict(
        title=None,
        released_on=None,
        developers=[],
        publishers=[],
        genres=[]
    )

    title_ndx = headers['title']
    for string in cells[title_ndx].strings:
        string = string.strip()
        if string and len(string) > 1:
            game['title'] = string
            break

    rel_ndx = headers['released_on']
    if cells[rel_ndx]:
        span = cells[rel_ndx].find(style='white-space:nowrap')
        if span:
            game['released_on'] = span.string.strip()
        elif cells[rel_ndx].find(string=True):
            game['released_on'] = cells[rel_ndx].find(string=True)

    for attr in ['developers', 'publishers', 'genres']:
        ndx = headers[attr]
        if ndx > 0:
            if cells[ndx]:
                for string in cells[ndx].strings:
                    string = string.strip()
                    if string and len(string) > 1:
                        string = re.sub(r',\s+Inc', ' Inc', string)
                        for val in re.split(r',|/', string):
                            val = val.strip()
                            if val:
                                game[attr].append(val)
                                # game[attr].append(string)

    return game


# ['game_id', 'title', 'released_on', 'price', 'pub_grp_id', 'plat_grp_id', 'dev_grp_id', 'genre_grp_id']
class SerializedVideoGame:
    def __init__(self, **kwargs):
        self.product_id = kwargs['product_id']
        self.title = kwargs['title']
        self.released_on = kwargs['released_on']
        self.price = kwargs['price']
        self.pub_grp_id = kwargs['pub_grp_id']
        self.plat_grp_id = kwargs['plat_grp_id']
        self.dev_grp_id = kwargs['dev_grp_id']
        self.genre_grp_id = kwargs['genre_grp_id']


def load(cachepath):
    games = []
    prodfile = path.join(cachepath, 'games.csv')
    with open(prodfile, 'rt') as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            games.append(SerializedVideoGame(
                product_id=int(row[0]),
                title=row[1],
                released_on=try_dtparse(row[2]),
                price=float(row[3]) if row[3] else None,
                pub_grp_id=int(row[4]) if row[4] else None,
                plat_grp_id=int(row[5]) if row[5] else None,
                dev_grp_id=int(row[6]) if row[6] else None,
                genre_grp_id=int(row[7]) if row[7] else None
            ))
    return sorted(games, key=lambda g: g.product_id)


def generate(outpath):
    pages = [
        ('xboxone', 'https://en.wikipedia.org/wiki/List_of_Xbox_One_games', xboxone_headers),
        ('xbox360', 'https://en.wikipedia.org/wiki/List_of_Xbox_360_games', xbox360_headers),
        ('xbox', 'https://en.wikipedia.org/wiki/List_of_Xbox_games', xbox_headers),
        ('xboxkinect', 'https://en.wikipedia.org/wiki/List_of_Kinect_games_for_Xbox_360', xboxkinect_headers),
        ('pc', 'https://en.wikipedia.org/wiki/List_of_PC_games', pc_headers),
        ('gamecube', 'https://en.wikipedia.org/wiki/List_of_Nintendo_GameCube_games', gamecube_headers),
        ('wii', 'https://en.wikipedia.org/wiki/List_of_Wii_games', wii_headers),
        ('nintendo3ds', 'https://en.wikipedia.org/wiki/List_of_Nintendo_3DS_games', nintendo3ds_headers),
        ('nintendods', 'https://en.wikipedia.org/wiki/List_of_Nintendo_DS_games_(A%E2%80%93I)', nintendods_headers),
        ('nintendods', 'https://en.wikipedia.org/wiki/List_of_Nintendo_DS_games_(J%E2%80%93R)', nintendods_headers),
        ('nintendods', 'https://en.wikipedia.org/wiki/List_of_Nintendo_DS_games_(S%E2%80%93Z)', nintendods_headers),
        ('ps4', 'https://en.wikipedia.org/wiki/List_of_PlayStation_4_games', ps4_headers),
        ('psp', 'https://en.wikipedia.org/wiki/List_of_PlayStation_Portable_games', psp_headers),
        ('psmove', 'https://en.wikipedia.org/wiki/List_of_PlayStation_Move_games', psmove_headers),
        ('psvita', 'https://en.wikipedia.org/wiki/List_of_PlayStation_Vita_games_(A%E2%80%93L)', psvita_headers),
        ('psvita', 'https://en.wikipedia.org/wiki/List_of_PlayStation_Vita_games_(M%E2%80%93Z)', psvita_headers),
        ('ps3', 'https://en.wikipedia.org/wiki/List_of_PlayStation_3_games_released_on_disc', ps3_headers),
        ('ps2', 'https://en.wikipedia.org/wiki/List_of_PlayStation_2_games', ps2_headers),
        ('ps1', 'https://en.wikipedia.org/wiki/List_of_PSone_Classics_(North_America)', psone_headers)
    ]
    platform_id_gen = IdGenerator()
    platforms = {
        'xbox': Platform(platform_id=platform_id_gen.generate(), name='Xbox'),
        'xbox360': Platform(platform_id=platform_id_gen.generate(), name='Xbox 360'),
        'xboxone': Platform(platform_id=platform_id_gen.generate(), name='Xbox One'),
        'xboxkinect': Platform(platform_id=platform_id_gen.generate(), name='Xbox Kinect'),
        'pc': Platform(platform_id=platform_id_gen.generate(), name='PC'),
        'gamecube': Platform(platform_id=platform_id_gen.generate(), name='Game Cube'),
        'wii': Platform(platform_id=platform_id_gen.generate(), name='Wii'),
        'nintendo3ds': Platform(platform_id=platform_id_gen.generate(), name='Nintendo 3DS'),
        'nintendods': Platform(platform_id=platform_id_gen.generate(), name='Nintendo DS'),
        'ps1': Platform(platform_id=platform_id_gen.generate(), name='Playstation 1'),
        'psmove': Platform(platform_id=platform_id_gen.generate(), name='Playstation Move'),
        'psvita': Platform(platform_id=platform_id_gen.generate(), name='Playstation Vita'),
        'psp': Platform(platform_id=platform_id_gen.generate(), name='PSP'),
        'ps4': Platform(platform_id=platform_id_gen.generate(), name='Playstation 4'),
        'ps3': Platform(platform_id=platform_id_gen.generate(), name='Playstation 3'),
        'ps2': Platform(platform_id=platform_id_gen.generate(), name='Playstation 2')
    }

    prod_id_gen = IdGenerator()
    products = {
        'halo2': VideoGame(product_id=prod_id_gen.generate()),
        'halo:combatevolved': VideoGame(product_id=prod_id_gen.generate()),
        'fable': VideoGame(product_id=prod_id_gen.generate()),
        "tomclancy'ssplintercell": VideoGame(product_id=prod_id_gen.generate()),
        'projectgothamracing': VideoGame(product_id=prod_id_gen.generate()),
        'grandtheftauto:doublepack': VideoGame(product_id=prod_id_gen.generate()),
        'starwars:knightsoftheoldrepublic': VideoGame(product_id=prod_id_gen.generate()),
        'counter-strike': VideoGame(product_id=prod_id_gen.generate()),
        'grandtheftauto:sanandreas': VideoGame(product_id=prod_id_gen.generate()),
        'needforspeed:underground2': VideoGame(product_id=prod_id_gen.generate()),
        'maddennfl06': VideoGame(product_id=prod_id_gen.generate()),
        'maddennfl07': VideoGame(product_id=prod_id_gen.generate()),
        'callofduty2:bigredone': VideoGame(product_id=prod_id_gen.generate()),
        'espnnfl2k5': VideoGame(product_id=prod_id_gen.generate()),
        'elderscrollsiii:morrowind!': VideoGame(product_id=prod_id_gen.generate()),
        'deadoralive3': VideoGame(product_id=prod_id_gen.generate()),
        'starwars:battlefrontii': VideoGame(product_id=prod_id_gen.generate()),
        'starwars:battlefront': VideoGame(product_id=prod_id_gen.generate()),
        "tomclancy'sghostrecon": VideoGame(product_id=prod_id_gen.generate()),
        'needforspeed:underground': VideoGame(product_id=prod_id_gen.generate()),
        'kinectadventures': VideoGame(product_id=prod_id_gen.generate()),
        'grandtheftautov': VideoGame(product_id=prod_id_gen.generate()),
        'halo3': VideoGame(product_id=prod_id_gen.generate()),
        'minecraft': VideoGame(product_id=prod_id_gen.generate()),
        'callofduty:blackops': VideoGame(product_id=prod_id_gen.generate()),
        'halo4': VideoGame(product_id=prod_id_gen.generate()),
        'gearsofwar': VideoGame(product_id=prod_id_gen.generate()),
        'gearsofwar2': VideoGame(product_id=prod_id_gen.generate()),
        'wiisportsresort': VideoGame(product_id=prod_id_gen.generate()),
        'newsupermariobros.2': VideoGame(product_id=prod_id_gen.generate()),
        'mariokartwii': VideoGame(product_id=prod_id_gen.generate()),
        'diabloiii': VideoGame(product_id=prod_id_gen.generate()),
        'elderscrollsv:skyrim,the': VideoGame(product_id=prod_id_gen.generate()),
        'sonicthehedgehog': VideoGame(product_id=prod_id_gen.generate()),
        'battlefield3': VideoGame(product_id=prod_id_gen.generate()),
        'grandtheftautoiii': VideoGame(product_id=prod_id_gen.generate()),
        'grandtheftauto:vicecity': VideoGame(product_id=prod_id_gen.generate()),
        'thesims': VideoGame(product_id=prod_id_gen.generate()),
        'thesims2': VideoGame(product_id=prod_id_gen.generate()),
        'wiifit': VideoGame(product_id=prod_id_gen.generate()),
        'supermario3dland': VideoGame(product_id=prod_id_gen.generate()),
        'brainage2:moretraininginminutesaday!': VideoGame(product_id=prod_id_gen.generate()),
        'callofduty:worldatwar:finalfronts': VideoGame(product_id=prod_id_gen.generate())
    }

    genre_id_gen = IdGenerator()
    genres = {}

    dev_id_gen = IdGenerator()
    devs = {}

    pub_id_gen = IdGenerator()
    pubs = {}

    for platform_name, url, headers in pages:
        try:
            logger.debug('Processing {}'.format(url))
            platform = platforms[platform_name]
            resp = requests.get(url)
            if resp.status_code != 200:
                logger.warn('Got status {} when fetching page {}. Skipping.'.format(resp.status_code, url))
                continue
            html = resp.text

            soup = BeautifulSoup(html, 'lxml')
            table = soup.find(id='mw-content-text').select('table.wikitable.sortable')[0]
            for row in table.find_all('tr'):
                try:
                    game = process(row, headers)
                    if game and game['title']:
                        title_key = re.sub(r'\s+', '', game['title'].lower())
                        if title_key in products:
                            product = products[title_key]
                            if not product.title:
                                product.title = game['title']
                            if not product.released_on:
                                product.released_on = try_dtparse(game['released_on'])
                            if not product.price:
                                product.price = np.random.randint(30, 80) + round(np.random.random(), 2)
                        else:
                            product = VideoGame(
                                product_id=prod_id_gen.generate(),
                                title=game['title'],
                                released_on=try_dtparse(game['released_on']),
                                price=np.random.randint(30, 80) + round(np.random.random(), 2)
                            )
                            products[title_key] = product

                        product.platforms.add(platform.platform_id)

                        for g in game['genres']:
                            genre_key = re.sub(r'\s+', '', g.lower())
                            if genre_key in genres:
                                genre = genres[genre_key]
                            else:
                                genre = Genre(genre_id=genre_id_gen.generate(), name=g)
                                genres[genre_key] = genre
                            product.genres.add(genre.genre_id)

                        for d in game['developers']:
                            dev_key = re.sub(r'\s+', '', d.lower())
                            if dev_key in devs:
                                dev = devs[dev_key]
                            else:
                                dev = Developer(developer_id=dev_id_gen.generate(), name=d)
                                devs[dev_key] = dev
                            product.developers.add(dev.developer_id)

                        for p in game['publishers']:
                            if re.fullmatch(r'\(*us\)*', p, re.IGNORECASE) \
                                    or re.fullmatch(r'\(*na\)*', p, re.IGNORECASE) \
                                    or re.fullmatch(r'\(*jp\)*', p, re.IGNORECASE) \
                                    or re.fullmatch(r'\(*eu\)*', p, re.IGNORECASE):
                                continue
                            pub_key = re.sub(r'\s+', '', p.lower())
                            if pub_key in pubs:
                                pub = pubs[pub_key]
                            else:
                                pub = Publisher(publisher_id=pub_id_gen.generate(), name=p)
                                pubs[pub_key] = pub
                            product.publishers.add(pub.publisher_id)
                except Exception as exp:
                    logger.error(row)
                    raise exp
        except Exception as e:
            logger.error('Got exception {} when processing page {}. Skipping.'.format(e, url))
            raise e
    logger.debug('Finished processing all pages')
    logger.debug('Got {} games'.format(len(products)))

    # games.csv: game_id, name, genre_grp_id, pub_grp_id, dev_grp_id, plat_grp_id
    # genres.csv: genre_id, name
    # genres_grp.csv: genre_grp_id, genre_id

    platgrps = dict()
    pg_id_gen = IdGenerator()

    devgrps = dict()
    dg_id_gen = IdGenerator()

    pubgrps = dict()
    bg_id_gen = IdGenerator()

    genregrps = dict()
    gg_id_gen = IdGenerator()

    for game in products.values():
        pgkey = ':'.join([str(i) for i in sorted(list(game.platforms))])
        if pgkey and pgkey not in platgrps:
            platgrps[pgkey] = pg_id_gen.generate()

        dgkey = ':'.join([str(i) for i in sorted(list(game.developers))])
        if dgkey and dgkey not in devgrps:
            devgrps[dgkey] = dg_id_gen.generate()

        bgkey = ':'.join([str(i) for i in sorted(list(game.publishers))])
        if bgkey and bgkey not in pubgrps:
            pubgrps[bgkey] = bg_id_gen.generate()

        ggkey = ':'.join([str(i) for i in sorted(list(game.genres))])
        if ggkey and ggkey not in genregrps:
            genregrps[ggkey] = gg_id_gen.generate()

    platfile = path.join(outpath, 'platforms.csv')
    with open(platfile, 'wt') as f1:
        writer = csv.writer(f1)
        # writer.writerow(['platform_id', 'name'])
        for platform in platforms.values():
            writer.writerow([platform.platform_id, platform.name])

    platgrpfile = path.join(outpath, 'plats_grp.csv')
    with open(platgrpfile, 'wt') as f11:
        writer = csv.writer(f11)
        # writer.writerow(['plat_grp_id', 'platform_id'])
        for key, i in platgrps.items():
            plat_ids = [int(s) for s in key.split(':')]
            for plat_id in plat_ids:
                writer.writerow([i, plat_id])

    genfile = path.join(outpath, 'genres.csv')
    with open(genfile, 'wt') as f2:
        writer = csv.writer(f2)
        # writer.writerow(['genre_id', 'name'])
        for genre in genres.values():
            writer.writerow([genre.genre_id, genre.name.lower()])

    gengrpfile = path.join(outpath, 'genres_grp.csv')
    with open(gengrpfile, 'wt') as f22:
        writer = csv.writer(f22)
        # writer.writerow(['genre_grp_id', 'genre_id'])
        for key, i in genregrps.items():
            genre_ids = [int(s) for s in key.split(':')]
            for genre_id in genre_ids:
                writer.writerow([i, genre_id])

    devfile = path.join(outpath, 'developers.csv')
    with open(devfile, 'wt') as f3:
        writer = csv.writer(f3)
        # writer.writerow(['developer_id', 'name'])
        for dev in devs.values():
            writer.writerow([dev.developer_id, dev.name])

    devgrpfile = path.join(outpath, 'devs_grp.csv')
    with open(devgrpfile, 'wt') as f33:
        writer = csv.writer(f33)
        # writer.writerow(['dev_grp_id', 'developer_id'])
        for key, i in devgrps.items():
            dev_ids = [int(s) for s in key.split(':')]
            for dev_id in dev_ids:
                writer.writerow([i, dev_id])

    pubfile = path.join(outpath, 'publishers.csv')
    with open(pubfile, 'wt') as f4:
        writer = csv.writer(f4)
        # writer.writerow(['publisher_id', 'name'])
        for pub in pubs.values():
            writer.writerow([pub.publisher_id, pub.name])

    pubgrpfile = path.join(outpath, 'pubs_grp.csv')
    with open(pubgrpfile, 'wt') as f44:
        writer = csv.writer(f44)
        # writer.writerow(['pub_grp_id', 'publisher_id'])
        for key, i in pubgrps.items():
            pub_ids = [int(s) for s in key.split(':')]
            for pub_id in pub_ids:
                writer.writerow([i, pub_id])

    prodfile = path.join(outpath, 'games.csv')
    with open(prodfile, 'wt') as f5:
        writer = csv.writer(f5)
        # writer.writerow(
        #     ['game_id', 'title', 'released_on', 'price', 'pub_grp_id', 'plat_grp_id', 'dev_grp_id', 'genre_grp_id'])
        for product in products.values():
            if not product.title:
                continue
            bgkey = ':'.join([str(i) for i in sorted(list(product.publishers))])
            bgid = pubgrps[bgkey] if bgkey in pubgrps else None
            pgkey = ':'.join([str(i) for i in sorted(list(product.platforms))])
            pgid = platgrps[pgkey] if pgkey in platgrps else None
            dgkey = ':'.join([str(i) for i in sorted(list(product.developers))])
            dgid = devgrps[dgkey] if dgkey in devgrps else None
            ggkey = ':'.join([str(i) for i in sorted(list(product.genres))])
            ggid = genregrps[ggkey] if ggkey in genregrps else None

            if product.released_on:
                released_on = product.released_on.strftime('%Y-%m-%d')
            else:
                released_on = None

            writer.writerow([product.product_id, product.title, released_on, product.price, bgid, pgid, dgid, ggid])

    return products.values()
