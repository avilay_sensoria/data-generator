import csv
import os.path as path
import numpy as np
import pkg_resources


def load(outpath):
    all_users = []
    outfile = path.join(outpath, 'users.csv')
    with open(outfile, 'rt') as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            all_users.append((row[0], row[1], row[2]))
    return all_users


def generate(num_males, num_females, outpath=None):
    """
    Will generate a csv with full names in the following format -
    first last,gender
    :param num_males: Number of male names to generate.
    :param num_females: Number of female names to generate.
    :param outpath: The path where the output csv is to be saved
    """
    male_fnames_path = pkg_resources.resource_filename(__package__, 'names/male_first_names.txt')
    all_male_fnames = np.genfromtxt(male_fnames_path, dtype=str)
    
    female_fnames_path = pkg_resources.resource_filename(__package__, 'names/female_first_names.txt')
    all_female_fnames = np.genfromtxt(female_fnames_path, dtype=str)

    lnames_path = pkg_resources.resource_filename(__package__, 'names/last_names.txt')
    all_lnames = np.genfromtxt(lnames_path, dtype=str)

    names = []
    for _ in range(num_males):
        fname = np.random.choice(all_male_fnames)
        lname = np.random.choice(all_lnames)
        names.append((fname + ' ' + lname, 'male'))

    for _ in range(num_females):
        fname = np.random.choice(all_female_fnames)
        lname = np.random.choice(all_lnames)
        names.append((fname + ' ' + lname, 'female'))

    if outpath:
        outfile = path.join(outpath, 'users.csv')
        with open(outfile, 'wt') as outfile:
            writer = csv.writer(outfile)
            # writer.writerow(['user_id', 'name', 'gender'])
            np.random.shuffle(names)
            for user_id, name in enumerate(names):
                user_id += 1
                writer.writerow([user_id, name[0], name[1]])

    return names
